last_wp_version := 5.0.3
last_wp_sha1 := f9a4b482288b5be7a71e9f3dc9b5b0c1f881102b
php_versions := 7.3
current_path := $(shell pwd)

define generate_tree
	mkdir -p $(current_path)/php$(3)/conf/fpm
	cp docker-entrypoint.sh $(current_path)/php$(3)/docker-entrypoint.sh
	cp wp-config.env.php $(current_path)/php$(3)/wp-config.env.php
	cp -r conf/fpm $(current_path)/php$(3)/conf
	sed -e 's/{WORDPRESS_VERSION}/$(1)/g' \
	-e 's/{PHP_VERSION}/$(3)/g' \
	-e 's/{WORDPRESS_SHA1}/$(4)/g' \
	Dockerfile.template > $(current_path)/php$(3)/Dockerfile

endef

define generate_job
	@echo "build-$(1)-php$(3):" >> .gitlab-ci.yml
	@echo "  <<: *job_definition" >> .gitlab-ci.yml
	@echo "  script:" >> .gitlab-ci.yml
	@echo "    - cd php$(3)" >> .gitlab-ci.yml
	@echo "    - docker build --pull -t \$$CONTAINER_RELEASE_IMAGE:$(1)-php$(3) ." >> .gitlab-ci.yml
	@echo "    - docker push \$$CONTAINER_RELEASE_IMAGE:$(1)-php$(3)" >> .gitlab-ci.yml
	@echo "" >> .gitlab-ci.yml

endef

all: clean dockerfiles .gitlab-ci.yml

.PHONY: dockerfiles
dockerfiles:
	$(foreach php, $(php_versions), \
		$(call generate_tree,$(last_wp_version),$(last_wp_cli_version),$(php),$(last_wp_sha1)) \
	)

.gitlab-ci.yml:
	@cp -f .gitlab-ci.template.yml .gitlab-ci.yml
	$(foreach php, $(php_versions), \
		$(call generate_job,$(last_wp_version),$(last_wp_cli_version),$(php)) \
	)

clean:
	-rm php* -rf
	-rm .gitlab-ci.yml

build: dockerfiles
	$(foreach php, $(php_versions), \
		cd $(current_path)/php$(php) && docker build . -t wordpress-image:$(last_wp_version)-$(php); \
	)
