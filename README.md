# WordPress docker images

[![pipeline status](https://gitlab.com/kalmac/wordpress/badges/master/pipeline.svg)]
(https://gitlab.com/kalmac/wordpress/commits/master)

1. [Description](#description)
2. [Available tags](#available-tags)
3. [Configuration](#configuration)
4. [Docker-Compose usage](#docker-compose-usage)
5. [Maintainers guide](#maintenairs-guide)

## Description

This docker image is built on [PHP FPM alpine](https://hub.docker.com/_/php).

Main benefits are :
* Images are smaller (11% less than official Wordpress images)
* Container is faster to start : Wordpress files are not copied or modified at start up.
* [WordPress constants](https://codex.wordpress.org/Editing_wp-config.php) can be defined with env variables.
* Files permissions allow PHP-FPM to write files and host user can share ownership to modify files without beeing root.

## Available tags

> **Pull command** :
> docker pull [registry.gitlab.com/kalmac/wordpress:5.0.3-php7.3](https://gitlab.com/kalmac/wordpress/container_registry)

- 5.0.3-php7.3

There is no *latest* image to avoid upgrading unintentionally.

## Configuration

Configuration is done by passing env variables at run command :
```
docker run -e USER_ID=1000 -d registry.gitlab.com/kalmac/wordpress
```

| Variable                           | Description                                                     | Default value |
|------------------------------------|-----------------------------------------------------------------|---------------|
| WORDPRESS_DB_USER                  | Database user (mandatory)                                       |               |
| WORDPRESS_DB_PASSWORD              | Database password (mandatory)                                   |               |
| WORDPRESS_DB_NAME                  | Database name (mandatory)                                       |               |
| WORDPRESS_DB_HOST                  | Database host or ip address                                     | db            |
| WORDPRESS_DB_COLLATE               | Database collation                                              | ""            |
| WORDPRESS_DB_CHARSET               | Database charset                                                | utf8mb4       |
| WORDPRESS_TABLE_PREFIX             | Wordress mysql tables prefix                                    | wp_           |
| USER_ID                            | Your user id (type id -u in a terminal).                        | 82            |
| FPM_MODE                           | PHP-FPM mode (ondemand, dynamic or static)                      | ondemand      |
| FPM_MAX_CHILDREN                   | PHP-FPM max children                                            | 5             |
| FPM_START_SERVERS                  | PHP_FPM start_servers                                           | 2             |
| WORDPRESS_DEBUG                    | WordPress `WP_DEBUG` constant                                   | "false"       |
| WORDPRESS_ALLOW_UNFILTERED_UPLOADS | WordPress `ALLOW_UNFILTERED_UPLOADS` constant                   | "false"       |
| WORDPRESS_AUTOSAVE_INTERVAL        | WordPress `AUTOSAVE_INTERVAL` constant                          | 60            |
| WORDPRESS_WP_POST_REVISIONS        | WordPress `WP_POST_REVISIONS` constant                          | 3             |
| WORDPRESS_FS_METHOD                | WordPress `FS_METHOD` constant                                  | "direct"      |
| WORDPRESS_WP_MAX_MEMORY_LIMIT      | WordPress `WP_MAX_MEMORY_LIMIT` constant                        | "256M"        |
| WORDPRESS_WP_MEMORY_LIMIT          | WordPress `WP_MEMORY_LIMIT` constant                            | "128M"        |
| WORDPRESS_CONCATENATE_SCRIPTS      | WordPress `CONCATENATE_SCRIPTS` constant                        | "true"        |
| WORDPRESS_DISABLE_WP_CRON          | WordPress `DISABLE_WP_CRON` constant                            | "false"       |
| WORDPRESS_DISALLOW_FILE_EDIT       | WordPress `DISALLOW_FILE_EDIT` constant                         | "true"        |
| WORDPRESS_DISALLOW_FILE_MODS       | WordPress `DISALLOW_FILE_MODS` constant                         | "true"        |
| WORDPRESS_FORCE_SSL_ADMIN          | WordPress `FORCE_SSL_ADMIN` constant                            | "false"       |
| WORDPRESS_WP_ALLOW_MULTISITE       | WordPress `WP_ALLOW_MULTISITE` constant                         | "false"       |
| WORDPRESS_WP_AUTO_UPDATE_CORE      | WordPress `WP_AUTO_UPDATE_CORE` constant                        | "false"       |
| WORDPRESS_WP_DEBUG_DISPLAY         | WordPress `WP_DEBUG_DISPLAY` constant                           | "false"       |
| WORDPRESS_WP_DEBUG_LOG             | WordPress `WP_DEBUG_LOG` constant                               | "false"       |
| WORDPRESS_SCRIPT_DEBUG             | WordPress `SCRIPT_DEBUG` constant                               | "false"       |
| WORDPRESS_SAVEQUERIES              | WordPress `SAVEQUERIES` constant                                | "false"       |

**FPM** parameters are set for dev and preproduction environments.
To use on production, use the *dynamic* PHP-FPM mode with correct max_children and start_servers values (see [here](https://medium.com/@sbuckpesch/apache2-and-php-fpm-performance-optimization-step-by-step-guide-1bfecf161534) and [here](https://guides.wp-bullet.com/adjusting-php-fpm-performance-low-memory/) to understand these parameters)

## Docker-compose usage

See [Starter/WordPress]() for documentations et complete examples.

## Maintainers guide

Pull this repository then edit the [Makefile](/Makefile) by adding the WordPress or php versions to support in the 3 first lines.

```Makefile
last_wp_version := 5.0.3
last_wp_sha1 := f9a4b482288b5be7a71e9f3dc9b5b0c1f881102b
php_versions := 7.3
```
> last_wp_version accepts only the last version to build.
> last_wp_sha1 should match the version downloaded
> php_versions is a list of versions to support separated by a space.

Run *make all* to generate the new Dockerfiles, add them to git.
Run *make build* to build a docker image "wordpress-image"

> Don’t forget to edit the [Available tags section](#available-tags) of this README.md file.

Once pushed to the *master* branch, the new images will be available in [registry](https://gitlab.com/kalmac/wordpress/container_registry).
