#!/bin/bash
set -euo pipefail

if [ -z ${WORDPRESS_DB_USER:-} ]; then
  echo >&2 "Error : missing WORDPRESS_DB_USER variable"
  WORDPRESS_DB_USER=""
fi
if [ -z ${WORDPRESS_DB_PASSWORD:-} ]; then
  echo >&2 "Error : missing WORDPRESS_DB_PASSWORD variable"
  WORDPRESS_DB_PASSWORD=""
fi
if [ -z ${WORDPRESS_DB_NAME:-} ]; then
  echo >&2 "Error : missing WORDPRESS_DB_NAME variable"
  WORDPRESS_DB_NAME=""
fi

if [ ! -f /var/www/html/wp-config-sample.php ]; then
    echo >&2 "Error : /var/www/html volume is recycled"
    touch /var/www/html/.recycled_volume
elif [ "$(id -u)" == "0" ]; then

  if [ ! -f /var/www/config/secret-keys ]; then
      echo >&2 "Generate secret keys from wordpress API ... please consider setting /var/www/config as a volume."
      mkdir -p /var/www/config/ && curl -o /var/www/config/secret-keys -fSL "https://api.wordpress.org/secret-key/1.1/salt/";
  fi

  echo >&2 "Replace env variables and move wp-config-sample.php"
  sed -i -e "s/database_name_here/${WORDPRESS_DB_NAME}/" \
    -e "s/username_here/${WORDPRESS_DB_USER}/" \
    -e "s/password_here/${WORDPRESS_DB_PASSWORD}/" \
    -e "s/localhost/${WORDPRESS_DB_HOST}/" \
    -e "s/'DB_COLLATE', ''/'DB_COLLATE', '${WORDPRESS_DB_COLLATE}'/" \
    -e "s/utf8/${WORDPRESS_DB_CHARSET}/" \
    -e "s/wp_/${WORDPRESS_TABLE_PREFIX}/" \
    -e "s/define('WP_DEBUG', false/define('WP_DEBUG', ${WORDPRESS_DEBUG}/" /var/www/html/wp-config-sample.php

  SECRET_FROM_LINE=$(grep -n "@+" /var/www/html/wp-config-sample.php | cut -d : -f 1)
  SECRET_TO_LINE=$(grep -n "@-" /var/www/html/wp-config-sample.php | cut -d : -f 1)
  head -n $(($SECRET_FROM_LINE-1)) /var/www/html/wp-config-sample.php > /var/www/html/wp-config.php
  cat /var/www/config/secret-keys >> /var/www/html/wp-config.php
  tail -n +$(($SECRET_TO_LINE+1)) /var/www/html/wp-config-sample.php >> /var/www/html/wp-config.php
  chown www-data:www-data /var/www/html/wp-config.php
  rm /var/www/html/wp-config-sample.php

  unset WORDPRESS_DB_PASSWORD
  unset WORDPRESS_DB_USER
  unset WORDPRESS_DB_NAME

  echo >&2 "Update php-fpm config"
  cp /usr/local/etc/php-fpm.d/all/www.conf /usr/local/etc/php-fpm.d/zz-www-custom.conf
  sed -i -e "s/{FPM_MODE}/${FPM_MODE}/" \
    -e "s/{FPM_MAX_CHILDREN}/${FPM_MAX_CHILDREN}/" \
    -e "s/{FPM_START_SERVERS}/${FPM_START_SERVERS}/" /usr/local/etc/php-fpm.d/zz-www-custom.conf

  if [ ! -z ${USER_ID:-} ]; then
    echo >&2 "Change www-data user id to $USER_ID"
    usermod -u $USER_ID www-data
  fi
else
  echo >&2 "Script runned as $(whoami)"
fi

exec "$@"
