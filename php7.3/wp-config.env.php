<?php
// String env constants.
$wpEnv = array(
    'WORDPRESS_AUTOSAVE_INTERVAL',
    'WORDPRESS_WP_POST_REVISIONS',
    'WORDPRESS_FS_METHOD',
    'WORDPRESS_WP_MAX_MEMORY_LIMIT',
    'WORDPRESS_WP_MEMORY_LIMIT',
);

/**
 * Sets WordPress constant (non boolean).
 *
 * @param string $envConstant Constant name.
 */
function set_wp_env($envConstant) {
    if ( false !== getEnv($envConstant) ) {
        define(str_replace('WORDPRESS_', '', $envConstant), getEnv($envConstant));
    }
}

foreach ($wpEnv as $wpConstant) {
    set_wp_env($wpConstant);
}

// Boolean env constants.
$wpEnvBool = array(
    'WORDPRESS_ALLOW_UNFILTERED_UPLOADS',
    'WORDPRESS_CONCATENATE_SCRIPTS',
    'WORDPRESS_DISABLE_WP_CRON',
    'WORDPRESS_DISALLOW_FILE_EDIT',
    'WORDPRESS_DISALLOW_FILE_MODS',
    'WORDPRESS_FORCE_SSL_ADMIN',
    'WORDPRESS_WP_ALLOW_MULTISITE',
    'WORDPRESS_WP_AUTO_UPDATE_CORE',
    'WORDPRESS_WP_DEBUG_DISPLAY',
    'WORDPRESS_WP_DEBUG_LOG',
    'WORDPRESS_SCRIPT_DEBUG',
    'WORDPRESS_SAVEQUERIES',
);

/**
 * Sets WordPress boolean constant.
 *
 * @param string $envConstant Constant name.
 */
function set_wp_boolean_env($envConstant) {
    if ( false !== getEnv($envConstant)) {
        if ( 'false' === getEnv($envConstant) ) {
            define(str_replace('WORDPRESS_', '', $envConstant), false);
        } elseif ( 'true' === getEnv($envConstant) ) {
            define(str_replace('WORDPRESS_', '', $envConstant), true);
        }
    }
}

foreach ($wpEnvBool as $wpConstantBool) {
    set_wp_boolean_env($wpConstantBool);
}

